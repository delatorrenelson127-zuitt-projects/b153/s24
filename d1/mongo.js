// MONGODB QUERY OPERATIONS & FIELD PROJECTION

/*
    $set - 
*/

db.users.updateOne(
    {
        _id: ObjectId("61fbc9af9d42a4a8386dd744")
    },
    {
        $set: {
            age: NumberInt(20)
        }
    }
)

// add 1 element to an existing array in MongoDB
db.users.updateOne(
    {
        _id: ObjectId("61fbc9af9d42a4a8386dd744")
    },
    {
        $push: {
            skills: 'CSS'
        }
    }
)

// add element to an existing array in MongoDB
db.users.updateOne(
    {
        _id: ObjectId("61fbc9af9d42a4a8386dd744")
    },
    {
        $push: {
            skills: {
                $each : ['CSS', 'NodeJS']
            }
        }
    }
)

// removing 'Field'
db.users.updateOne(
    {
        _id: ObjectId("61fbc9af9d42a4a8386dd744")
    },
    {
        $unset:{
            skills: ""
        }
    }
)

db.users.insertMany([
    {
        name : 'Jane Doe',
        age : NumberInt(31),
        isActive : true,
        skills:['HTML', 'CSS', 'Bootstrap']
    },
    {
        name : 'Markov Wallowitz',
        age : NumberInt(34),
        isActive : true,
        skills:['HTML', 'CSS', 'JavaScript']
    },
    {
        name : 'Nick Callowy',
        age : NumberInt(43),
        isActive : true,
        skills:['HTML','MERN']
    },
    {
        name : 'Martha Sky',
        age : NumberInt(45),
        isActive : true,
        skills:['HTML', 'PHP', 'MySQL']
    },
    {
        name : 'Lillith Washington',
        age : NumberInt(32),
        isActive : true,
        skills:['JavaScript', 'Python']
    },
])

// Show listings that are of property type 'condominium,' has a minimum stay of 1 nigth, has 1 bedroom and can accommodate 2 people
db.listingsAndReviews.find(
  {
    property_type: "Condominium",
    minimum_nights: "2",
    bedrooms: 1,
    accommodates: 2,
  },
  {
    name: 1,
    property_type: 1,
    bedrooms: 1,
    accommdates: 0,
  }
);

// Displaying Range
db.listingsAndReviews.find(
    {
        price: {
            $lt: 100
        },
        extra_people:{
            lt:20
        },
        'address.country':'Portugal'
    },
    {
        name: 1,
        price: 1,
        extra_people:1,
        'address.country':1
    }
).limit(5)


// increase the price of the previous 5 listing by 10
db.listingsAndReviews.updateMany(
    {
        price: {
            $lt: 100
        }
    },
    {
        $inc:{
            price: 10
        }
    }
)

// delete listings with any one of the following conditions:
// review scores accuracy <=75
// review scores cleanliness <= 80
// review scores checkin <= 70
// review scores communication <= 70
// review scores location <= 75
// review scores value <= 80
db.listingsAndReviews.deleteMany(
    {
      $or:
      [ 
        {"review_scores.review_scores_accuracy": {$lte: 75}},
        {"review_scores.review_scores_cleanliness": {$lte: 80}},
        {"review_scores.review_scores_checkin": {$lte: 70}},
        {"review_scores.review_scores_communication": {$lte: 70}},
        {"review_scores.review_scores_location": {$lte: 75}},
        {"review_scores.review_scores_value": {$lte: 80}}
      ]
    }
  )


